<?php

namespace App\Entity;

use App\Traits\Commentable;
use App\Traits\Sellable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class ProductSold {
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=256)
	 */
	private $image;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="productsSold")
	 */
	private $user;

	/**
	 * the stack of products for one client at one time
	 * @ORM\ManyToOne(targetEntity="SellRecord", inversedBy="productsSold")
	 */
	public $sellRecords;

	/**
	 * references the product from whom this line is inspired
	 * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productsSold")
	 */
	private $product;

    #[ORM\ManyToOne(inversedBy: 'productSold')]
    private ?Admin $admin = null;

	use Sellable;
	use Commentable;


	/**
	 * Set sellRecords.
	 *
	 * @param \App\Entity\SellRecord|null $sellRecords
	 *
	 * @return ProductSold
	 */
	public function setSellRecords( \App\Entity\SellRecord $sellRecords = null ) {
         		$this->sellRecords = $sellRecords;
         
         		return $this;
         	}

	/**
	 * Get sellRecords.
	 *
	 * @return \App\Entity\SellRecord|null
	 */
	public function getSellRecords() {
         		return $this->sellRecords;
         	}

	/**
	 * Set product.
	 *
	 * @param \App\Entity\Product|null $product
	 *
	 * @return ProductSold
	 */
	public function setProduct( \App\Entity\Product $product = null ) {
         		$this->product = $product;
         
         		return $this;
         	}

	/**
	 * Get product.
	 *
	 * @return \App\Entity\Product|null
	 */
	public function getProduct() {
         		return $this->product;
         	}

	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
         		return $this->id;
         	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return ProductSold
	 */
	public function setName( $name ) {
         		$this->name = $name;
         
         		return $this;
         	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
         		return $this->name;
         	}

	/**
	 * Set image.
	 *
	 * @param string $image
	 *
	 * @return ProductSold
	 */
	public function setImage( $image ) {
         		$this->image = $image;
         
         		return $this;
         	}

	/**
	 * Get image.
	 *
	 * @return string
	 */
	public function getImage() {
         		return $this->image;
         	}

	/**
	 * Set user.
	 *
	 * @param \App\Entity\User|null $user
	 *
	 * @return ProductSold
	 */
	public function setUser( \App\Entity\User $user = null ) {
         		$this->user = $user;
         
         		return $this;
         	}

	/**
	 * Get user.
	 *
	 * @return \App\Entity\User|null
	 */
	public function getUser() {
         		return $this->user;
         	}

    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    public function setAdmin(?Admin $admin): static
    {
        $this->admin = $admin;

        return $this;
    }
}
