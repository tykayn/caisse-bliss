<?php

namespace App\Entity;

use App\Traits\Commentable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SellRecordRepository")
 */
class SellRecord {

	use Commentable;
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * gender of the client
	 * @ORM\Column( type = "string", nullable=true )
	 */
	private $gender;
	/**
	 * liste des produits de la vente
	 * @ORM\OneToMany(targetEntity="App\Entity\ProductSold", mappedBy="sellRecords", cascade={"remove", "persist","detach"})
	 */
	private $productsSold;


	/**
	 * creation date
	 * @ORM\Column(type="datetime")
	 */
	private $date;

	/**
	 * total
	 * @ORM\Column(type="decimal", scale=2, nullable=false)
	 */
	private $amount;
	/**
	 * amount paid by client
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $paidByClient;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="App\Entity\Festival",inversedBy="sellRecords")
	 */
	private $festival;

	/**
	 * owner of the selling account
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="sellRecords")
	 */
	private $user;

    #[ORM\ManyToOne(inversedBy: 'sellRecord')]
    private ?Admin $admin = null;


	/**
	 * Constructor
	 */
	public function __construct() {
         		$this->productsSold = new \Doctrine\Common\Collections\ArrayCollection();
         	}

	/**
	 * @return mixed
	 */
	public function getId() {
         		return $this->id;
         	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
         		$this->id = $id;
         	}

	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return SellRecord
	 */
	public function setDate( $date ) {
         		$this->date = $date;
         
         		return $this;
         	}

	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate() {
         		return $this->date;
         	}

	/**
	 * Set amount
	 *
	 * @param string $amount
	 *
	 * @return SellRecord
	 */
	public function setAmount( $amount ) {
         		$this->amount = $amount;
         
         		return $this;
         	}

	/**
	 * Get amount
	 *
	 * @return string
	 */
	public function getAmount() {
         		return $this->amount;
         	}

	/**
	 * Set paidByClient
	 *
	 * @param string $paidByClient
	 *
	 * @return SellRecord
	 */
	public function setPaidByClient( $paidByClient ) {
         		$this->paidByClient = $paidByClient;
         
         		return $this;
         	}

	/**
	 * Get paidByClient
	 *
	 * @return string
	 */
	public function getPaidByClient() {
         		return $this->paidByClient;
         	}

	/**
	 * Add productsSold
	 *
	 * @param \App\Entity\ProductSold $productsSold
	 *
	 * @return SellRecord
	 */
	public function addProductsSold( \App\Entity\ProductSold $productsSold ) {
         		$this->productsSold[] = $productsSold;
         
         		return $this;
         	}

	/**
	 * Remove productsSold
	 *
	 * @param \App\Entity\ProductSold $productsSold
	 */
	public function removeProductsSold( \App\Entity\ProductSold $productsSold ) {
         		$this->productsSold->removeElement( $productsSold );
         	}

	/**
	 * Get productsSold
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProductsSold() {
         		return $this->productsSold;
         	}

	/**
	 * Set festival
	 *
	 * @param \App\Entity\Festival $festival
	 *
	 * @return SellRecord
	 */
	public function setFestival( \App\Entity\Festival $festival = null ) {
         		$this->festival = $festival;
         
         		return $this;
         	}

	/**
	 * Get festival
	 *
	 * @return \App\Entity\Festival
	 */
	public function getFestival() {
         		return $this->festival;
         	}

	/**
	 * Set user
	 *
	 * @param \App\Entity\User $user
	 *
	 * @return SellRecord
	 */
	public function setUser( \App\Entity\User $user = null ) {
         		$this->user = $user;
         
         		return $this;
         	}

	/**
	 * Get user
	 *
	 * @return \App\Entity\User
	 */
	public function getUser() {
         		return $this->user;
         	}

    /**
     * Set gender.
     *
     * @param string|null $gender
     *
     * @return SellRecord
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    public function setAdmin(?Admin $admin): static
    {
        $this->admin = $admin;

        return $this;
    }
}
