<?php

namespace App\Entity;

use App\Traits\Commentable;
use App\Traits\Sellable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
#[ApiResource]
class Product {

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
	private $id;
	/**
	 * number of items available
	 * @ORM\Column(name="stock_count", type="integer")
	 */
	private $stockCount;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;


	/**
	 * url for image
	 * @ORM\Column(type="string", length=256, nullable=true)
	 */
	private $image;

	/**
	 * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
	 */
	private $category;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="products")
	 */
	private $user;
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\ProductSold", mappedBy="product", cascade={"remove"})
	 */
	private $productsSold;

    #[ORM\ManyToMany(targetEntity: Admin::class, mappedBy: 'products')]
    private Collection $admins;

	use Sellable;
	use Commentable;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
                     		return $this->id;
                     	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Product
	 */
	public function setName( $name ) {
                     		$this->name = $name;
                     
                     		return $this;
                     	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
                     		return $this->name;
                     	}

	/**
	 * Set image.
	 *
	 * @param string|null $image
	 *
	 * @return Product
	 */
	public function setImage( $image = null ) {
                     		$this->image = $image;
                     
                     		return $this;
                     	}

	/**
	 * Get image.
	 *
	 * @return string|null
	 */
	public function getImage() {
                     		return $this->image;
                     	}

	/**
	 * Set category.
	 *
	 * @param \App\Entity\ProductCategory|null $category
	 *
	 * @return Product
	 */
	public function setCategory( \App\Entity\ProductCategory $category = null ) {
                     		$this->category = $category;
                     
                     		return $this;
                     	}

	/**
	 * Get category.
	 *
	 * @return \App\Entity\ProductCategory|null
	 */
	public function getCategory() {
                     		return $this->category;
                     	}

	/**
	 * Set user.
	 *
	 * @param \App\Entity\User|null $user
	 *
	 * @return Product
	 */
	public function setUser( \App\Entity\User $user = null ) {
                     		$this->user = $user;
                     
                     		return $this;
                     	}

	/**
	 * Get user.
	 *
	 * @return \App\Entity\User|null
	 */
	public function getUser() {
                     		return $this->user;
                     	}

	/**
	 * Set stockCount.
	 *
	 * @param int $stockCount
	 *
	 * @return Product
	 */
	public function setStockCount( $stockCount ) {
                     		$this->stockCount = $stockCount;
                     
                     		return $this;
                     	}

	/**
	 * Get stockCount.
	 *
	 * @return int
	 */
	public function getStockCount() {
                     		return $this->stockCount;
                     	}

	/**
	 * Constructor
	 */
	public function __construct() {
                     		$this->productsSold = new \Doctrine\Common\Collections\ArrayCollection();
                       $this->admins = new ArrayCollection();
                     	}

	/**
	 * Add productsSold.
	 *
	 * @param \App\Entity\User $productsSold
	 *
	 * @return Product
	 */
	public function addProductsSold( \App\Entity\User $productsSold ) {
                     		$this->productsSold[] = $productsSold;
                     
                     		return $this;
                     	}

	/**
	 * Remove productsSold.
	 *
	 * @param \App\Entity\User $productsSold
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeProductsSold( \App\Entity\User $productsSold ) {
                     		return $this->productsSold->removeElement( $productsSold );
                     	}

	/**
	 * Get productsSold.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProductsSold() {
                     		return $this->productsSold;
                     	}

    /**
     * @return Collection<int, Admin>
     */
    public function getAdmins(): Collection
    {
        return $this->admins;
    }

    public function addAdmin(Admin $admin): static
    {
        if (!$this->admins->contains($admin)) {
            $this->admins->add($admin);
            $admin->addProduct($this);
        }

        return $this;
    }

    public function removeAdmin(Admin $admin): static
    {
        if ($this->admins->removeElement($admin)) {
            $admin->removeProduct($this);
        }

        return $this;
    }
}
