<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductCategoryRepository")
 */
class ProductCategory {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"remove"})
	 */
	private $products;
	/**
	 * @ORM\OneToMany(targetEntity="ProductSold", mappedBy="product", cascade={"remove"})
	 */
	private $productsSold;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="categories")
	 */
	private $users;

    #[ORM\ManyToOne(inversedBy: 'productCategory')]
    private ?Admin $admin = null;

	public function __toString() {
         		return $this->getName() . ' (' . count( $this->getProducts() ) . ' produits)';
         	}

	/**
	 * @param $userId
	 *
	 * @return bool
	 */
	public function hasUser( $userId ) {
         		foreach ( $this->getUsers() as $user ) {
         			if ( $user->getId() === $userId ) {
         				return true;
         			}
         		}
         
         		return false;
         	}

	/**
	 * @return mixed
	 */
	public function getUsers() {
         		return $this->users;
         	}

	/**
	 * @param mixed $users
	 */
	public function setUsers( $users ) {
         		$this->users = $users;
         	}

	/**
	 * @return mixed
	 */
	public function getProducts() {
         		return $this->products;
         	}

	/**
	 * @param mixed $products
	 */
	public function setProducts( $products ) {
         		$this->products = $products;
         	}

	/**
	 * @return mixed
	 */
	public function getId() {
         		return $this->id;
         	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
         		$this->id = $id;
         	}

	/**
	 * @return mixed
	 */
	public function getName() {
         		return $this->name;
         	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
         		$this->name = $name;
         	}

	/**
	 * Constructor
	 */
	public function __construct() {
         		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
         	}

	/**
	 * Add product
	 *
	 * @param \App\Entity\Product $product
	 *
	 * @return ProductCategory
	 */
	public function addProduct( \App\Entity\Product $product ) {
         		$this->products[] = $product;
         
         		return $this;
         	}

	/**
	 * Remove product
	 *
	 * @param \App\Entity\Product $product
	 */
	public function removeProduct( \App\Entity\Product $product ) {
         		$this->products->removeElement( $product );
         	}

	/**
	 * Add user
	 *
	 * @param \App\Entity\User $user
	 *
	 * @return ProductCategory
	 */
	public function addUser( \App\Entity\User $user ) {
         		$this->users[] = $user;
         
         		return $this;
         	}

	/**
	 * Remove user
	 *
	 * @param \App\Entity\User $user
	 */
	public function removeUser( \App\Entity\User $user ) {
         		$this->users->removeElement( $user );
         	}

    /**
     * Add productsSold.
     *
     * @param \App\Entity\ProductSold $productsSold
     *
     * @return ProductCategory
     */
    public function addProductsSold(\App\Entity\ProductSold $productsSold)
    {
        $this->productsSold[] = $productsSold;

        return $this;
    }

    /**
     * Remove productsSold.
     *
     * @param \App\Entity\ProductSold $productsSold
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProductsSold(\App\Entity\ProductSold $productsSold)
    {
        return $this->productsSold->removeElement($productsSold);
    }

    /**
     * Get productsSold.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsSold()
    {
        return $this->productsSold;
    }

    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    public function setAdmin(?Admin $admin): static
    {
        $this->admin = $admin;

        return $this;
    }
}
