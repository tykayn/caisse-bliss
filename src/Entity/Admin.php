<?php

namespace App\Entity;

use App\Repository\AdminRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdminRepository::class)]
class Admin
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $username = null;

    #[ORM\Column(length: 255)]
    private ?string $email = null;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'admins')]
    private Collection $products;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: Festival::class)]
    private Collection $festival;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: ProductCategory::class)]
    private Collection $productCategory;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: ProductSold::class)]
    private Collection $productSold;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: SellRecord::class)]
    private Collection $sellRecord;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: SerieFestival::class)]
    private Collection $serieFestivals;

    #[ORM\OneToMany(mappedBy: 'admin', targetEntity: ExpenseKind::class)]
    private Collection $expenseKind;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->festival = new ArrayCollection();
        $this->productCategory = new ArrayCollection();
        $this->productSold = new ArrayCollection();
        $this->sellRecord = new ArrayCollection();
        $this->serieFestivals = new ArrayCollection();
        $this->expenseKind = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): static
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
        }

        return $this;
    }

    public function removeProduct(Product $product): static
    {
        $this->products->removeElement($product);

        return $this;
    }

    /**
     * @return Collection<int, Festival>
     */
    public function getFestival(): Collection
    {
        return $this->festival;
    }

    public function addFestival(Festival $festival): static
    {
        if (!$this->festival->contains($festival)) {
            $this->festival->add($festival);
            $festival->setAdmin($this);
        }

        return $this;
    }

    public function removeFestival(Festival $festival): static
    {
        if ($this->festival->removeElement($festival)) {
            // set the owning side to null (unless already changed)
            if ($festival->getAdmin() === $this) {
                $festival->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductCategory>
     */
    public function getProductCategory(): Collection
    {
        return $this->productCategory;
    }

    public function addProductCategory(ProductCategory $productCategory): static
    {
        if (!$this->productCategory->contains($productCategory)) {
            $this->productCategory->add($productCategory);
            $productCategory->setAdmin($this);
        }

        return $this;
    }

    public function removeProductCategory(ProductCategory $productCategory): static
    {
        if ($this->productCategory->removeElement($productCategory)) {
            // set the owning side to null (unless already changed)
            if ($productCategory->getAdmin() === $this) {
                $productCategory->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductSold>
     */
    public function getProductSold(): Collection
    {
        return $this->productSold;
    }

    public function addProductSold(ProductSold $productSold): static
    {
        if (!$this->productSold->contains($productSold)) {
            $this->productSold->add($productSold);
            $productSold->setAdmin($this);
        }

        return $this;
    }

    public function removeProductSold(ProductSold $productSold): static
    {
        if ($this->productSold->removeElement($productSold)) {
            // set the owning side to null (unless already changed)
            if ($productSold->getAdmin() === $this) {
                $productSold->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SellRecord>
     */
    public function getSellRecord(): Collection
    {
        return $this->sellRecord;
    }

    public function addSellRecord(SellRecord $sellRecord): static
    {
        if (!$this->sellRecord->contains($sellRecord)) {
            $this->sellRecord->add($sellRecord);
            $sellRecord->setAdmin($this);
        }

        return $this;
    }

    public function removeSellRecord(SellRecord $sellRecord): static
    {
        if ($this->sellRecord->removeElement($sellRecord)) {
            // set the owning side to null (unless already changed)
            if ($sellRecord->getAdmin() === $this) {
                $sellRecord->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SerieFestival>
     */
    public function getSerieFestivals(): Collection
    {
        return $this->serieFestivals;
    }

    public function addSerieFestival(SerieFestival $serieFestival): static
    {
        if (!$this->serieFestivals->contains($serieFestival)) {
            $this->serieFestivals->add($serieFestival);
            $serieFestival->setAdmin($this);
        }

        return $this;
    }

    public function removeSerieFestival(SerieFestival $serieFestival): static
    {
        if ($this->serieFestivals->removeElement($serieFestival)) {
            // set the owning side to null (unless already changed)
            if ($serieFestival->getAdmin() === $this) {
                $serieFestival->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ExpenseKind>
     */
    public function getExpenseKind(): Collection
    {
        return $this->expenseKind;
    }

    public function addExpenseKind(ExpenseKind $expenseKind): static
    {
        if (!$this->expenseKind->contains($expenseKind)) {
            $this->expenseKind->add($expenseKind);
            $expenseKind->setAdmin($this);
        }

        return $this;
    }

    public function removeExpenseKind(ExpenseKind $expenseKind): static
    {
        if ($this->expenseKind->removeElement($expenseKind)) {
            // set the owning side to null (unless already changed)
            if ($expenseKind->getAdmin() === $this) {
                $expenseKind->setAdmin(null);
            }
        }

        return $this;
    }
}
