<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function home(): Response
    {
        return $this->render('default/index.html.twig', [

        ]);
    }
    #[Route('/default', name: 'app_default')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
    #[Route('/lucky/number', name: 'app_default')]
    public function number(): Response
    {

        $number = random_int(0, 100);

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'number' => $number
        ]);
    }

}
