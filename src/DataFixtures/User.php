<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User as UserClass;

class User extends Fixture
{
    public function load(ObjectManager $manager): void
    {
         $demo_user = new UserClass();

         $demo_user
             ->setLogin('demo')
             ->setPass('demo')
         ;
         $manager->persist($demo_user);

        $manager->flush();
    }
}
