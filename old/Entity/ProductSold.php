<?php

namespace AppBundle\Entity;

use AppBundle\Traits\Commentable;
use AppBundle\Traits\Sellable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class ProductSold {
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=256)
	 */
	private $image;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="productsSold")
	 */
	private $user;

	/**
	 * the stack of products for one client at one time
	 * @ORM\ManyToOne(targetEntity="SellRecord", inversedBy="productsSold")
	 */
	public $sellRecords;

	/**
	 * references the product from whom this line is inspired
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="productsSold")
	 */
	private $product;

	use Sellable;
	use Commentable;


	/**
	 * Set sellRecords.
	 *
	 * @param \AppBundle\Entity\SellRecord|null $sellRecords
	 *
	 * @return ProductSold
	 */
	public function setSellRecords( \AppBundle\Entity\SellRecord $sellRecords = null ) {
		$this->sellRecords = $sellRecords;

		return $this;
	}

	/**
	 * Get sellRecords.
	 *
	 * @return \AppBundle\Entity\SellRecord|null
	 */
	public function getSellRecords() {
		return $this->sellRecords;
	}

	/**
	 * Set product.
	 *
	 * @param \AppBundle\Entity\Product|null $product
	 *
	 * @return ProductSold
	 */
	public function setProduct( \AppBundle\Entity\Product $product = null ) {
		$this->product = $product;

		return $this;
	}

	/**
	 * Get product.
	 *
	 * @return \AppBundle\Entity\Product|null
	 */
	public function getProduct() {
		return $this->product;
	}

	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return ProductSold
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set image.
	 *
	 * @param string $image
	 *
	 * @return ProductSold
	 */
	public function setImage( $image ) {
		$this->image = $image;

		return $this;
	}

	/**
	 * Get image.
	 *
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Set user.
	 *
	 * @param \AppBundle\Entity\User|null $user
	 *
	 * @return ProductSold
	 */
	public function setUser( \AppBundle\Entity\User $user = null ) {
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user.
	 *
	 * @return \AppBundle\Entity\User|null
	 */
	public function getUser() {
		return $this->user;
	}
}
