<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductCategoryRepository")
 */
class ProductCategory {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\OneToMany(targetEntity="Product", mappedBy="category", cascade={"remove"})
	 */
	private $products;
	/**
	 * @ORM\OneToMany(targetEntity="ProductSold", mappedBy="product", cascade={"remove"})
	 */
	private $productsSold;

	/**
	 * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="categories")
	 */
	private $users;

	public function __toString() {
		return $this->getName() . ' (' . count( $this->getProducts() ) . ' produits)';
	}

	/**
	 * @param $userId
	 *
	 * @return bool
	 */
	public function hasUser( $userId ) {
		foreach ( $this->getUsers() as $user ) {
			if ( $user->getId() === $userId ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getUsers() {
		return $this->users;
	}

	/**
	 * @param mixed $users
	 */
	public function setUsers( $users ) {
		$this->users = $users;
	}

	/**
	 * @return mixed
	 */
	public function getProducts() {
		return $this->products;
	}

	/**
	 * @param mixed $products
	 */
	public function setProducts( $products ) {
		$this->products = $products;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->products = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add product
	 *
	 * @param \AppBundle\Entity\Product $product
	 *
	 * @return ProductCategory
	 */
	public function addProduct( \AppBundle\Entity\Product $product ) {
		$this->products[] = $product;

		return $this;
	}

	/**
	 * Remove product
	 *
	 * @param \AppBundle\Entity\Product $product
	 */
	public function removeProduct( \AppBundle\Entity\Product $product ) {
		$this->products->removeElement( $product );
	}

	/**
	 * Add user
	 *
	 * @param \AppBundle\Entity\User $user
	 *
	 * @return ProductCategory
	 */
	public function addUser( \AppBundle\Entity\User $user ) {
		$this->users[] = $user;

		return $this;
	}

	/**
	 * Remove user
	 *
	 * @param \AppBundle\Entity\User $user
	 */
	public function removeUser( \AppBundle\Entity\User $user ) {
		$this->users->removeElement( $user );
	}

    /**
     * Add productsSold.
     *
     * @param \AppBundle\Entity\ProductSold $productsSold
     *
     * @return ProductCategory
     */
    public function addProductsSold(\AppBundle\Entity\ProductSold $productsSold)
    {
        $this->productsSold[] = $productsSold;

        return $this;
    }

    /**
     * Remove productsSold.
     *
     * @param \AppBundle\Entity\ProductSold $productsSold
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeProductsSold(\AppBundle\Entity\ProductSold $productsSold)
    {
        return $this->productsSold->removeElement($productsSold);
    }

    /**
     * Get productsSold.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsSold()
    {
        return $this->productsSold;
    }
}
