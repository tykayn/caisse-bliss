<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpenseKind, for previsional compta
 *
 * @ORM\Table(name="expense_kind")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpenseKindRepository")
 */
class ExpenseKind
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="delay", type="integer", nullable=true)
     */
    private $delay;
    /**
     * line enabled to calculate on
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;


    /**
     * @var int|null
     *
     * @ORM\Column(name="repeatitions", type="integer", nullable=true)
     */
    private $repeatitions;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="expenses")
     */
    private $user;

    /**
     * @return int|null
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param int|null $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ExpenseKind
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set delay.
     *
     * @param int|null $delay
     *
     * @return ExpenseKind
     */
    public function setDelay($delay = null)
    {
        $this->delay = $delay;

        return $this;
    }

    /**
     * Get delay.
     *
     * @return int|null
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * Set repeatitions.
     *
     * @param int|null $repeatitions
     *
     * @return ExpenseKind
     */
    public function setRepeatitions($repeatitions = null)
    {
        $this->repeatitions = $repeatitions;

        return $this;
    }

    /**
     * Get repeatitions.
     *
     * @return int|null
     */
    public function getRepeatitions()
    {
        return $this->repeatitions;
    }

    /**
     * Set amount.
     *
     * @param float $amount
     *
     * @return ExpenseKind
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
