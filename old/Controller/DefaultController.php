<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ExpenseKind;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\ProductSold;
use AppBundle\Entity\SellRecord;
use AppBundle\Service\OwnerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller {

	private $ownerService;
	private $tokenManager;

	public function __construct( CsrfTokenManagerInterface $tokenManager = null, OwnerService $ownerService ) {
		$this->tokenManager = $tokenManager;
		$this->ownerService = $ownerService;
	}

	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction( Request $request ) {
		$m        = $this->getDoctrine()->getManager();
		$userRepo = $m->getRepository( 'AppBundle:User' );
		$allUsers = $userRepo->findAll();

		// replace this example code with whatever you need
		return $this->render( 'default/index.html.twig',
			[
				'usersCount' => count( $allUsers ),
				'base_dir'   => realpath( $this->getParameter( 'kernel.project_dir' ) ) . DIRECTORY_SEPARATOR,
			] );
	}

	/**
	 * @Route("/dashboard", name="dashboard")
	 */
	public function dashboardAction( Request $request ) {
		$m           = $this->getDoctrine()->getManager();
		$currentUser = $this->getUser();

// TODO on first login set default values
		$lastFestival = $currentUser->getActiveFestival();
		if ( ! $lastFestival ) {
			$lastFestival = $m->getRepository( 'AppBundle:Festival' )
			                  ->findOneBy( [ 'user' => $this->getUser()->getId() ],
				                  [ 'id' => 'desc' ],
				                  0,
				                  1 );
		}
		if ( $lastFestival ) {
			$lastFestival->recalculateChiffreAffaire();
		}
		$categRepo   = $m->getRepository( 'AppBundle:ProductCategory' );
		$sellingRepo = $m->getRepository( 'AppBundle:SellRecord' );
		$categories  = $categRepo->findAll();
		$recentSells = $sellingRepo->findBy( [ 'user' => $currentUser->getId() ], [ 'id' => 'desc' ], 0, 5 );

		return $this->render( 'logged/dashboard.html.twig',
			[
				'lastFestival' => $lastFestival,
				'categories'   => $categories,
				'currentUser'  => $currentUser,
				'recentSells'  => $recentSells,
				'base_dir'     => realpath( $this->getParameter( 'kernel.project_dir' ) ) . DIRECTORY_SEPARATOR,
			] );
	}


	/**
	 * envoyer un email
	 */
	public function emailAction() {
		$name    = "noble barbare";
		$message = \Swift_Message::newInstance()
		                         ->setSubject( 'Hello Email' )
		                         ->setFrom( 'test-symfony-tykayn@caisse.ciperbliss.com' )
		                         ->setTo( 'tykayn@gmail.com' )
		                         ->setBody( $this->renderView( 'default/test-email.html.twig' ),
			                         'text/html' );
		$this->get( 'mailer' )->send( $message );

//return 'yay test de mail';
		return $this->render(
			'default/test-email.html.twig',
			[
			]
		);
	}

	/**
	 * get user products
	 * @return JsonResponse
	 */
	public function getMyProductsAction() {
		$m = $this->getDoctrine()->getManager();


		$currentUser = $this->getUser();
		if ( ! $currentUser ) {
			return new JsonResponse( [
				'categories'  => [ [] ],
				'recentSells' => [ [ '' ] ],
			] );
		}

		$ownerService = $this->ownerService;
		$ownerService->setupNewFestival( $currentUser );
		$activeFestival = $currentUser->getActiveFestival();

		$categRepo   = $m->getRepository( 'AppBundle:ProductCategory' );
		$sellingRepo = $m->getRepository( 'AppBundle:SellRecord' );


		$categories = $ownerService->serializeCategoriesOfUser( $currentUser );


		$recentSells = $sellingRepo->findBy( [ 'user' => $currentUser->getId() ], [ 'id' => 'desc' ], 0, 5 );

		return new JsonResponse( [
			'categories'   => $categories,
			'recentSells'  => count( $recentSells ),
			'lastFestival' => $activeFestival->makeArray(),
		] );
	}

	/**
	 * get user expenses
	 * @return JsonResponse
	 */
	public function getMyExpensesAction() {
		$m = $this->getDoctrine()->getManager();


		$currentUser = $this->getUser();
		if ( ! $currentUser ) {
			return new JsonResponse( [
				'expenses' => [ [] ],
			] );
		}

		$ownerService = $this->ownerService;
		$ownerService->setupNewFestival( $currentUser );
		$expensesOfUser = $ownerService->serializeExpensesOfUser( $currentUser );

		return new JsonResponse( [
			'expenses'               => $expensesOfUser,
			'disponibility'          => $currentUser->getDisponibility(),
			'averageMonthlyEarnings' => $currentUser->getAverageMonthlyEarnings(),
		] );
	}

	/**
	 * recieve the json containing the expanse config of a user
	 *
	 * @param Request $request
	 *
	 * @return JsonResponse the list of expanses
	 */
	public function saveMyExpensesAction( Request $request ) {

		$json           = json_decode( $request->getContent(), true );
		$currentUser    = $this->getUser();
		$m              = $this->getDoctrine()->getManager();
		$myExpenses     = $currentUser->getExpenses();
		$categoriesByID = [];

		foreach ( $myExpenses as $expense ) {
			$categoriesByID[ $expense->getId() ] = $expense;
		}
		// loop on the json config for expanse
		// save the user configuration
		foreach ( $json[ 'expenses' ] as $expens ) {

			if ( isset( $expens[ 'id' ] ) ) {
				$foundExpense = $categoriesByID[ $expens[ 'id' ] ];
				if ( $foundExpense ) {
					// update existing expenses of logged in user
					$foundExpense->setName( $expens[ 'name' ] );
					$foundExpense->setAmount( $expens[ 'amount' ] );
					$foundExpense->setDelay( $expens[ 'delay' ] );
					$foundExpense->setRepeatitions( $expens[ 'repeat' ] );
					$foundExpense->setEnabled( $expens[ 'enabled' ] );
					$m->persist( $foundExpense );
				}
			} else {
				// create new expense for user
				$newExpense = new ExpenseKind();
				$newExpense->setUser( $currentUser );
				$newExpense->setName( $expens[ 'name' ] );
				$newExpense->setDelay( $expens[ 'delay' ] );
				$newExpense->setAmount( $expens[ 'amount' ] );
				$newExpense->setRepeatitions( $expens[ 'repeat' ] );
				$newExpense->setEnabled( $expens[ 'enabled' ] );
				$m->persist( $newExpense );

			}
		}

		$currentUser->setDisponibility( $json[ 'config' ][ 'disponibility' ] );
		$currentUser->setAverageMonthlyEarnings( $json[ 'config' ][ 'averageMonthlyEarnings' ] );


		$m->persist( $currentUser );
		$m->flush();

		$ownerService   = $this->ownerService;
		$expensesOfUser = $ownerService->serializeExpensesOfUser( $currentUser );

		return new JsonResponse( [
			'expenses'               => $expensesOfUser,
			'disponibility'          => $currentUser->getDisponibility(),
			'averageMonthlyEarnings' => $currentUser->getAverageMonthlyEarnings(),
		] );
	}

	/**
	 * @param Request $request
	 * add a selling record corresponding to one client
	 *
	 * @return JsonResponse
	 */
	public function addSellingAction( Request $request ) {

		$json          = json_decode( $request->getContent(), true );
		$currentUser   = $this->getUser();
		$m             = $this->getDoctrine()->getManager();
		$newSellRecord = new SellRecord();
		// sort user categories
		$myCategories   = $currentUser->getCategories();
		$categoriesByID = [];
		foreach ( $myCategories as $my_category ) {
			$categoriesByID[ $my_category->getId() ] = $my_category;
		}

		$productsModels     = $m->getRepository( 'AppBundle:Product' )->findAll();
		$productsModelsByID = [];
		foreach ( $productsModels as $product ) {
			$productsModelsByID[ $product->getId() ] = $product;
		}

		$sumAmount = 0;
		foreach ( $json[ 'activeSelling' ] as $record ) {
			$productModel   = $productsModelsByID[ $record[ 'id' ] ];
			$newProductSold = new ProductSold();
			$newProductSold->setName( $record[ 'name' ] );
			$newProductSold->setImage( "image mock" );
			$newProductSold->setUser( $currentUser );
			$newProductSold->setPrice( $record[ 'price' ] );
			$newProductSold->setComment( $json[ 'sellingComment' ] );
			$newProductSold->setProduct( $productModel );
			$newProductSold->setSellRecords( $newSellRecord );
			// link selling record with user, festival
			$currentUser->addProductsSold( $newProductSold );
			// persist all
			$productModel->setStockCount( $productModel->getStockCount() - 1 );
			$m->persist( $productModel );
			$m->persist( $newProductSold );
			$m->persist( $newProductSold );
			$sumAmount += $record[ 'price' ];
		}

		$festivalFound = $m->getRepository( 'AppBundle:Festival' )->find( $json[ 'activeFestival' ][ 'id' ] );
		$newSellRecord->setFestival( $festivalFound );
		$newSellRecord->setAmount( $sumAmount );
		$newSellRecord->setDate( new \DateTime() );
		$newSellRecord->setUser( $currentUser );
		$newSellRecord->setPaidByClient( $json[ 'paidByClient' ] );
		$newSellRecord->setComment( $json[ 'sellingComment' ] );


		$festivalFound->addSellRecord( $newSellRecord );
		$currentUser->addSellRecords( $newSellRecord );

		$m->persist( $newSellRecord );
		$m->persist( $currentUser );
		$m->persist( $festivalFound );
		$m->flush();

		$festivalFound->recalculateChiffreAffaire();
		$m->persist( $festivalFound );
		$m->flush();

		// setup dates

		// fetch back history of selling
		$sellingRepo  = $m->getRepository( 'AppBundle:SellRecord' );
		$lastSellings = $sellingRepo->findBy( [ 'user' => $currentUser->getId() ], [ 'id' => 'desc' ], 0, 3 );

		return new JsonResponse( [
			"message"           => "ok",
			"activeFestival"    => $festivalFound->makeArray(),
			"newChiffreAffaire" => $festivalFound->getChiffreAffaire(),
			"clientsCount"      => count( $festivalFound->getSellRecords() ),
			"recent_sellings"   => json_encode( $lastSellings ),
			"dump"              => $json,
		], 200 );
	}


	/**
	 * get the history of user's sellings
	 * @Route("/history", name="history")
	 */
	public function historyAction() {
		$currentUser     = $this->getUser();
		$m               = $this->getDoctrine()->getManager();
		$sellingRepo     = $m->getRepository( 'AppBundle:SellRecord' );
		$allSellingList  = $sellingRepo->findBy( [ 'user' => $currentUser->getId() ], [ 'id' => 'desc' ] );
		$mySellings      = array_splice( $allSellingList, 0, 15 );
		$chiffreAffaires = 0;

		$myFestivals = $currentUser->getFestivals();

		$statisticsFestivals = [
		];

		foreach ( $myFestivals as $festival ) {
			$statisticsFestivals[] = [
				'date'           => $festival->getDateCreation(),
				'name'           => $festival->getName(),
				'clients_count'  => count( $festival->getSellRecords() ),
				'chiffreAffaire' => $festival->getChiffreAffaire(),
			];
		}
		$statisticsSoldProducts = [
//            ['name' => 'bidule', 'count' => 0, 'value' => 0],
		];

		$statsForFestivalMock = [
			[ 'name' => 'festoche bidule', 'clients_count' => 125, 'chiffreAffaire' => 236, 'date' => new \DateTime() ],
		];
		foreach ( $allSellingList as $client ) {
			foreach ( $client->getProductsSold() as $product ) {
				$chiffreAffaires += $product->getPrice();
			}
		}
		foreach ( $mySellings as $client ) {
			foreach ( $client->getProductsSold() as $product ) {
				$chiffreAffaires += $product->getPrice();

				if ( ! isset( $statisticsSoldProducts[ $product->getName() ] ) ) {
					$statisticsSoldProducts[ $product->getName() ] =
						[
							'name'  => $product->getName(),
							'count' => 0,
							'value' => 0,
						];
				}

				$statisticsSoldProducts[ $product->getName() ][ 'count' ] ++;
				$statisticsSoldProducts[ $product->getName() ][ 'value' ] = $statisticsSoldProducts[ $product->getName() ][ 'value' ] + intval( $product->getPrice() );

			}
		}

		return $this->render( 'logged/history.html.twig',
			[
//                'statisticsFestivals' => $statsForFestivalMock, // mock of festival stats
				'statisticsFestivals'    => $statisticsFestivals,
				'statisticsSoldProducts' => $statisticsSoldProducts,
				'chiffreAffaires'        => $chiffreAffaires,
				'recentSells'            => $mySellings,
				'allSellings'            => count( $mySellings ),
				'base_dir'               => realpath( $this->getParameter( 'kernel.project_dir' ) ) . DIRECTORY_SEPARATOR,
			] );
	}

	/**
	 * export user data in JSON
	 * @return JsonResponse
	 * @Route("/logged/export-all-json", name="export_all_json")
	 */
	public function exportJsonAction() {
		$currentUser = $this->getUser();
		$m           = $this->getDoctrine()->getManager();
		$sellingRepo = $m->getRepository( 'AppBundle:SellRecord' );
		$encoders    = [ new XmlEncoder(), new JsonEncoder() ];
		$normalizers = [ new ObjectNormalizer() ];

		$serializer = new Serializer( $normalizers, $encoders );

		$mySellings = $sellingRepo->findByUser( $currentUser->getId() );
		$export     = [
			'export_version' => '1.0',
			'user'           => $serializer->normalize( $currentUser,
				null,
				[ 'attributes' => [ 'id', 'username', 'email', 'salt', 'password' ] ] ),
			'products'       => $serializer->normalize( $currentUser->getProducts(),
				null,
				[ 'attributes' => [ 'id', 'name', 'price' ] ] ),
			'categories'     => $serializer->normalize( $currentUser->getCategories(),
				null,
				[ 'attributes' => [ 'id', 'name' ] ] ),
			'series_festivals'      => $serializer->normalize( $currentUser->getSeriesFestivals(),
null, ['attributes'=> [
					'id', 'name',
				]]),
			'festivals'      => $serializer->normalize( $currentUser->getFestivals(),
				null,
				[ 'attributes' => [ 'id', 'name', 'chiffreAffaire','fraisInscription','fraisHebergement','fraisTransport','fraisRepas' ] ] ),
			'sellings'       => $serializer->normalize( $mySellings,
				null,
				[ 'attributes' => [ 'id', 'amount', 'paidByClient', 'comment', 'gender' ] ] ),
		];

		return new JsonResponse( $export );
	}

	/**
	 * export all clients
	 * @Route("/export-all", name="export_all")
	 */
	public function exportAllAction() {
		// TODO
		$currentUser = $this->getUser();
		$m           = $this->getDoctrine()->getManager();
		$sellingRepo = $m->getRepository( 'AppBundle:SellRecord' );
		$mySellings  = $sellingRepo->findByUser( $currentUser->getId() );

		$fileName = "export_caisse-cipherbliss_" . $currentUser->getUsername() . '_' . date( 'Y-m-d_H-i-s' );

		$handle    = fopen( 'php://memory', 'r+' );
		$firstLine = [
			'product sold id',
			'date',
			'hour',
			'min',
			'sec',
			'client comment',
			'client paid',
			'product name',
			'product image',
			'product category id',
			'product category name',
			'product price',
			'festival id',
			'festival name',
			'festival creation date',
			'caisse before',
			'caisse after',
			'festival ca',
			'festival comment',
		];
// save the column headers
		fputcsv( $handle,
			$firstLine );

		$chiffreAffaires = 0;
		foreach ( $mySellings as $sellRecord ) {
			foreach ( $sellRecord->getProductsSold() as $productSold ) {
				$chiffreAffaires += $productSold->getPrice();
				// add a line to the csv file
				fputcsv( $handle,
					[
						$productSold->getId(),
						$sellRecord->getDate()->format( 'c' ),
						$sellRecord->getDate()->format( 'H' ),
						$sellRecord->getDate()->format( 'i' ),
						$sellRecord->getDate()->format( 's' ),
						$sellRecord->getComment(),
						$sellRecord->getPaidByClient(),
						$productSold->getName(),
						$productSold->getImage(),
						$productSold->getProduct()->getCategory()->getId(),
						$productSold->getProduct()->getCategory()->getName(),
						$productSold->getPrice(),
					] );
				if ( $sellRecord->getFestival() ) {
					fputcsv( $handle,
						[
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							$sellRecord->getFestival()->getId(),
							$sellRecord->getFestival()->getName(),
							$sellRecord->getFestival()->getDateCreation()->format( 'c' ),
							$sellRecord->getFestival()->getFondDeCaisseAvant(),
							$sellRecord->getFestival()->getFondDeCaisseApres(),
							$sellRecord->getFestival()->getChiffreAffaire(),
							$sellRecord->getFestival()->getComment(),
						] );
				}

			}
		}
		rewind( $handle );
		$content = stream_get_contents( $handle );
		fclose( $handle );

		return new Response(
			$content, 200, [
				'Content-Type'        => 'application/force-download',
				'Content-Disposition' => 'attachment; filename="' . $fileName . '.csv"',
			]
		);
	}


	/**
	 * @Route("/set-active-festival/{id}", name="set_active_festival")
	 */
	public function setActiveFestivalAction( $id ) {
		$currentUser = $this->getUser();
		$m           = $this->getDoctrine()->getManager();
		$repo        = $m->getRepository( 'AppBundle:Festival' );

		$currentUser->setActiveFestival( $repo->find( $id ) );
		$m->persist( $currentUser );
		$m->flush();

		// replace this example code with whatever you need
		return $this->redirectToRoute( 'festival_index' );
	}

	/**
	 * @Route("/import", name="import")
	 */
	public function importAction() {
		$currentUser = $this->getUser();
		$m           = $this->getDoctrine()->getManager();
		$sellingRepo = $m->getRepository( 'AppBundle:SellRecord' );

		return $this->render( 'logged/import.html.twig',
			[
				'base_dir' => '',
			] );
	}

	/**
	 * @Route("/previsionnel", name="previsionnel")
	 */
	public function previsionnelAction() {
//        $currentUser = $this->getUser();
//        $m = $this->getDoctrine()->getManager();
//        $sellingRepo = $m->getRepository('AppBundle:SellRecord');

		return $this->render( 'logged/previsionnel.html.twig',
			[
				'base_dir' => '',
			] );
	}

	/**
	 * import lots of products at once
	 * @Route("/mass-create", name="mass_create")
	 */
	public function massCreateAction( Request $request ) {
		$currentUser = $this->getUser();
		$m           = $this->getDoctrine()->getManager();

		$myCategories       = $currentUser->getCategories();
		$myCategoriesByName = [];
		$myProductsByName   = [];
		$currentCategory    = new ProductCategory();
		$currentCategory
			->addUser( $currentUser )
			->setName( 'default category' );

		foreach ( $myCategories as $my_category ) {
			$myCategoriesByName [ $my_category->getName() ] = $my_category;
			foreach ( $my_category->getProducts() as $product ) {
				$myProductsByName[ $product->getName() ] = $product;
			}
			$currentCategory = $my_category;
		}
		$massLines = $request->request->get( 'produits' );
		if ( $request->getMethod() == 'POST' ) {
			$lines = preg_split( '/$\R?^/m', trim( $massLines ) );
			foreach ( $lines as $line ) {

				if ( strpos( $line, ':' ) ) {
					// manage catgegories
					$boom      = explode( ':', trim( $line ) );
					$firstPart = $boom[ 0 ];
					$value     = $boom[ 1 ];
					if ( $firstPart === 'catégorie' && $value ) {
						// look for category by name
						if ( ! isset( $myCategoriesByName[ $value ] ) ) {
							$newCateg = new ProductCategory();
							$newCateg
								->addUser( $currentUser )
								->setName( $value );
							$currentUser->addCategory( $newCateg );
							$m->persist( $newCateg );
							$currentCategory = $newCateg; // associate further categories with the newly created one
						} else {
//                            echo " la catégorie existe déjà";
						}
					}
				} else {
					// manage product
					$boom        = explode( ';', $line );
					$productName = $boom[ 0 ];
					$price       = 0;
					if ( $boom[ 1 ] ) {
						$price = intval( str_replace( '€', '', $boom[ 1 ] ) );// removing euro symbol
					}
					// or create new product
					if ( $productName && ! isset( $myProductsByName[ $productName ] ) ) {
						$newProduct = new Product();
						$newProduct->setCategory( $currentCategory )
						           ->setName( $productName )
						           ->setStockCount( 500 )
						           ->setUser( $currentUser )
						           ->setPrice( $price );
						$currentUser->addProduct( $newProduct );
						$m->persist( $newProduct );
					}// look for existing products
					else {
						$myProductsByName[ $productName ]->setPrice( $price );
					}
				}

				$m->persist( $currentUser );
			}
			// check with existing categories and products, sort them by name.
			// save all
			$m->flush();
		}

		return $this->render( 'logged/import.html.twig',
			[] );
	}

}
