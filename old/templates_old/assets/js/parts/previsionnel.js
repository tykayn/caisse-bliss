export var PrevisionnelCtrl =  function ($scope, $http, $timeout) {

    $scope.config = {
        initialLoadingDone    : false,
        loading               : false,
        lines                 : 24,
        debounceTime          : 300, // miliseconds to wait before updating model and saving changes
        /**
         * expenses kind of the user
         */
        disponibility         : 5000,
        averageMonthlyEarnings: 600,
        warningThershold      : 2000,
        showDelays            : false,
        showRepeats           : false,
        monthsBeforeNoMoney   : null,
    };

    let exampleExpenses = [
        {name: "appart", amount: 800, delay: 0, repeat: $scope.config.lines, enabled: true},
        {name: "assurance voiture", amount: 50, delay: 0, repeat: $scope.config.lines, enabled: true},
        {name: "internet", amount: 20, delay: 0, repeat: $scope.config.lines, enabled: true},
        {name: "elec", amount: 100, delay: 0, repeat: $scope.config.lines, enabled: true},
        {name: "chat", amount: 20, delay: 0, repeat: $scope.config.lines, enabled: true},
        {name: "transports", amount: 70, delay: 0, repeat: $scope.config.lines, enabled: false},
    ];

    // $scope.expenses=[];
    $scope.expenses = exampleExpenses;

    /**
     * sum of all monthly expenses, ignoring delay
     * @returns {number}
     */
    $scope.sumMonthlyExpenses = () => {
        let sum = 0;
        $scope.expenses.forEach((elem) => {
            if (elem.enabled) {
                sum += elem.amount;
            }
        })
        return sum;
    };
    $scope.previsionTable = [];
    $scope.calculatePrevisionTable = () => {
        let turns = $scope.config.lines;
        let monthly = $scope.sumMonthlyExpenses();
        let available = $scope.config.disponibility;
        let previsionTable = [];
        let changedNoMoneyConfig = false;
        $scope.config.monthsBeforeNoMoney = null;
        for (let i = 0; i <= turns; i++) {
            // TODO take in account delays in expenses
            available = available - monthly + $scope.config.averageMonthlyEarnings;
            let newLine = {
                expense  : monthly,
                available: available,
            };

            if (available <= 0 && !changedNoMoneyConfig) {
                $scope.config.monthsBeforeNoMoney = i;
                changedNoMoneyConfig = true;
            }
            previsionTable.push(newLine);
        }
        $scope.previsionTable = previsionTable;
        $scope.makeGraphPointsOfPrevisionTable(previsionTable);
        return previsionTable;
    };
    $scope.graphPointsPrevision = [];
    $scope.makeGraphPointsOfPrevisionTable = (previsionTable) => {
        console.log("previsionTable", previsionTable);
        $scope.graphPointsPrevision = [];
        for (let i = 0; i < previsionTable.length; i++) {
            $scope.graphPointsPrevision.push({
                label: previsionTable[i].available + " euros restants dans " + i + " mois",
                y    : previsionTable[i].available,
                x    : i,
            })
        }

    }

    $scope.updateconf = (rep) => {
        // update view calculs
        $scope.calculatePrevisionTable();
        $scope.updateCanevas()
        // flags
        $scope.config.loading = false;
        $scope.config.initialLoadingDone = true;
        $scope.config.disponibility = rep.data.disponibility;
        $scope.config.averageMonthlyEarnings = rep.data.averageMonthlyEarnings;
        // default data when user has nothing saved
        console.log('rep.data.expenses.length', rep.data.expenses.length)
        if (!rep.data.expenses.length) {
            $scope.expenses = exampleExpenses;
        } else {
            $scope.expenses = rep.data.expenses;
        }
    };
    // http related calls
    $scope.fetchExpenses = () => {
        console.log('fetch expenses...');
        $scope.config.loading = true;

        $http.get('get-my-expenses').then((rep) => {
              console.log('get-my-expenses', rep.data.expenses);
              $scope.updateconf(rep)
          },
          $scope.manageError)
    };
    $scope.save = function () {
        if ($scope.config.loading) {
            console.log('already saving');
            return;
        }
        console.log('update expenses...');
        $scope.config.loading = true;
        $http.post('save-my-expenses', {
            expenses: $scope.expenses,
            config  : $scope.config
        })
          .then((rep) => {
                console.log('save-my-expenses', rep);
                $scope.updateconf(rep)
            },
            $scope.manageError)
    };
    $scope.addExpense = function () {
        $scope.expenses.push({
            name  : "",
            repeat: 0,
            delay : 0,
            amount: 0,
        })
    };
    $scope.init = function () {
        $scope.fetchExpenses();
    };
    $scope.manageError = (error) => {
        console.error(error);
        $scope.config.loading = false;

    }
    $scope.updateCanevas = function () {
        var dataPoints = $scope.graphPointsPrevision;
        var chartContainer = new CanvasJS.Chart("simulationPrevision", {
            title: {
                text: "Euros disponibles dans le temps"
            },
            // animationEnabled: true,
            data : [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type      : "splineArea",
                    dataPoints: dataPoints
                }
            ]
        });
        chartContainer.render();
    }
    $scope.init();
};
