

let introJs = require('intro.js');
var $ = require('jquery');
$(document).ready(function () {
    let massimportExample = 'catégorie: livre\n' +
      '    les moutaines;5€\n' +
      '    la laine des moutons;6€\n' +
      '    star wars spécial noël;7€\n' +
      'catégorie: poster\n' +
      '    super bannière A2;10€\n' +
      '    Sébastien Chabal sexy;10€\n' +
      'catégorie: dessin à la demande\n' +
      '    dessin A4 crayon;20€\n' +
      '    dessin A4 aquarelle;150€';

    $('[data-toggle="popover"]').popover();
    $('#menu_button').on('click', function () {
        $('#menu-dashboard').toggleClass('shown');
    });
    // use example in mass import
    $('#use_example').on('click', function () {
        $('#produits').val(massimportExample);
        // $('#filling_zone input').click();
    });
    // intro js
    $('#introjs_start').on('click', function () {

        function startIntro() {
            var intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: document.querySelector('#caisse-now'),
                        position: 'right',
                        intro  : "Ceci est l'écran principal de la CaisseBliss, <strong>votre tableau de bord</strong> liste les produits que vous souhaitez vendre actuellement. Utilisez les flèches du clavier pour continuer la visite guidée."
                    },
                    {
                        element: document.querySelector('#main_options'),
                        position: 'bottom',
                        intro  : "Les options d'affichage des boutons de produit permettent de visualiser ou non les stocks restants de vos produits, le nombre de produit vendus sur le festival en cours, et d'activer ou non le bouton de caddie de <strong>vente express</strong> à utiliser pour les clients qui n'achètent qu'un seul produit à la fois."
                    },
                    {
                        element: document.querySelector('#client-now'),
                        position: 'bottom',
                        intro  : "<strong>L'espace commentaire </strong> lié à la vente en cours vous permet de prendre une note que vous retrouverez plus tard dans votre historique de vente. Il peut servir à savoir à qui vous faites une dédicace dans un livre par exemple."
                    },{
                        element: document.querySelector('#listing-products'),
                        intro  : "<strong>La liste des produits </strong> les range par colonne de catégorie. Vous n'avez qu'a appuyer sur un produit pour l'ajouter à la liste de vente en cours. Et vous pouvez scroller horizontalement vos colonnes"
                    },{
                        element: document.querySelector('#sellings'),
                        intro  : "<strong>La liste de vente </strong> permet de modifier le prix des produits si vous faites des remises spéciales pour un client particulier, et vous calcule le rendu de monnaie si le client ne paie pas l'appoint."
                    },{
                        element: document.querySelector('#choice-categories'),
                        intro  : "<strong>Les options de catégories </strong> permettent d'afficher ou masquer certaines catégories si vous ne vendez pas tous vos produits à chaque festival."
                    },
                    {
                        element: document.querySelector('#menu-dashboard'),
                        intro  : "<strong>Le menu latéral</strong> vous permet de gérer vos produits, stocks, catégories. Conforme au RGPD par design de logiciel libre, Caisse Bliss vous permet d'exporter vos données et respecte votre vie privée"
                    },
                    {
                        element: document.querySelector('#menu_festivals'),
                        position: 'right',
                        intro  : "<strong>Les festivals</strong> vous permettent de séparer vos ventes lors de différents évènements et d'obtenir des statistiques détaillées afin de vous permettre de connaître la rentabilité de chaque évènement"
                    },
                    {
                        element: document.querySelector('#menu_series'),
                        position: 'right',
                        intro  : "<strong>Les séries de festivals</strong> permettent de relier des évènements similaires d'une édition à une autre. Par exemple Festival Harajuku 2018 et 2019, Japan Expo de toutes les années. Et ce afin de pouvoir voir des statistiques groupées dans la partie Historique."
                    },
                    {
                        element: document.querySelector('#menu_previsionnel'),
                        position: 'right',
                        intro  : "<strong>La page de prévisionnel</strong> n'est pas reliée à vos ventes de produits mais permet de voir si votre commerce est viable en fonction de ce que vous gagnez et dépensez habituellement. Toujours utile."
                    }, {
                        element: document.querySelector('.user-info-link'),
                        position: 'right',
                        intro  : "Vous pouvez modifier vos informations de compte utilisateur (email, pseudo, mot de passe...) en cliquant sur votre pseudo"
                    },
                ]
            });

            intro.start();
        }
        startIntro()
    })

    // demo login
    $('#demo_login_btn').on('click', function (){
        $('#username').val('demo');
        $('#password').val('demo');
        $('#_submit').click();

    })
    // $('#introjs_start').click();
    let inputClient = document.querySelector('.client-now input');
    inputClient ? inputClient.focus() : false;
});
