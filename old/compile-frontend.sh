#/bin/bash
echo "###############################";
echo "start update of frontend submodule assets";
echo "###############################";
git submodule update;
cd caisse-bliss-frontend
ng build
cp caisse-bliss-frontend/dist/appli/* /assets/js/build-frontend-submodule
echo "###########";
echo "update done";
echo "###########";
