<?php

namespace CaisseBliss\Entity;

use CaisseBliss\Traits\Commentable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Festival
 *
 * @ORM\Table(name="festival")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FestivalRepository")
 */
class Festival {

	use Commentable;
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="dateCreation", type="datetime")
	 */
	private $dateCreation;

	/**
	 * @var \stdClass
	 *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\SellRecord",mappedBy="festival", cascade={"remove"})
	 */
	private $sellRecords;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="festivals")
	 */
	private $user;
	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SerieFestival", inversedBy="festivals")
	 */
	private $serieFestival;

	/**
	 * @var
	 * @ORM\Column(name="fond_de_caisse_avant", type="float")
	 */
	private $fondDeCaisseAvant;

	/**
	 * @var
	 * @ORM\Column(name="fond_de_caisse_apres", type="float")
	 */
	private $fondDeCaisseApres;
	/**
	 * @var
	 * @ORM\Column(name="chiffre_affaire", type="float")
	 */
	private $chiffreAffaire;
	/**
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $fraisInscription;
	/**
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $fraisHebergement;
	/**
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $fraisTransport;
	/**
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $fraisRepas;


	public function __toString() {
		return $this->getName();
	}

	/**
     * @return mixed
     */
    public function getSerieFestival()
    {
        return $this->serieFestival;
    }

    /**
     * @param mixed $serieFestival
     */
    public function setSerieFestival($serieFestival)
    {
        $this->serieFestival = $serieFestival;
    }

	/**
	 * array usable by js
	 * @return array
	 */
	public function makeArray(){
		$sellRecords = $this->getSellRecords();
		$soldItems = [];
		foreach ( $sellRecords as $sell_record ) {
			foreach ( $sell_record->getProductsSold() as $sold ) {
				if(!isset($soldItems[$sold->getProduct()->getId()])){
					$soldItems[$sold->getProduct()->getId()] = 0;
				}
				$soldItems[$sold->getProduct()->getId()]++;
			}

		}
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'commentaire' => $this->getComment(),
			'dateCreation' => $this->getDateCreation(),
			'chiffreAffaire' => $this->getChiffreAffaire(),
			'clientsCount' => count($this->getSellRecords()),
			'fondDeCaisseAvant' => $this->getFondDeCaisseAvant(),
			'fondDeCaisseApres' => $this->getFondDeCaisseApres(),
			'sold' => $soldItems,
		];

	}
	public function recalculateChiffreAffaire() {
		$sellings          = $this->getSellRecords();
		$newChiffreAffaire = 0;
		foreach ( $sellings as $selling ) {
			$newChiffreAffaire += $selling->getAmount();
		}

		$this->setChiffreAffaire( $newChiffreAffaire );

		return $this;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Festival
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set dateCreation
	 *
	 * @param \DateTime $dateCreation
	 *
	 * @return Festival
	 */
	public function setDateCreation( $dateCreation ) {
		$this->dateCreation = $dateCreation;

		return $this;
	}

	/**
	 * Get dateCreation
	 *
	 * @return \DateTime
	 */
	public function getDateCreation() {
		return $this->dateCreation;
	}

	/**
	 * Set sellRecords
	 *
	 * @param \stdClass $sellRecords
	 *
	 * @return Festival
	 */
	public function setSellRecords( $sellRecords ) {
		$this->sellRecords = $sellRecords;

		return $this;
	}

	/**
	 * Get sellRecords
	 *
	 * @return \stdClass
	 */
	public function getSellRecords() {
		return $this->sellRecords;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->sellRecords = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add sellRecord
	 *
	 * @param \CaisseBliss\Entity\SellRecord $sellRecord
	 *
	 * @return Festival
	 */
	public function addSellRecord( \CaisseBliss\Entity\SellRecord $sellRecord ) {
		$this->sellRecords[] = $sellRecord;

		return $this;
	}

	/**
	 * Remove sellRecord
	 *
	 * @param \CaisseBliss\Entity\SellRecord $sellRecord
	 */
	public function removeSellRecord( \CaisseBliss\Entity\SellRecord $sellRecord ) {
		$this->sellRecords->removeElement( $sellRecord );
	}

	/**
	 * Set user.
	 *
	 * @param \CaisseBliss\Entity\User|null $user
	 *
	 * @return Festival
	 */
	public function setUser( \CaisseBliss\Entity\User $user = null ) {
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user.
	 *
	 * @return \CaisseBliss\Entity\User|null
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set fondDeCaisseAvant.
	 *
	 * @param float $fondDeCaisseAvant
	 *
	 * @return Festival
	 */
	public function setFondDeCaisseAvant( $fondDeCaisseAvant ) {
		$this->fondDeCaisseAvant = $fondDeCaisseAvant;

		return $this;
	}

	/**
	 * Get fondDeCaisseAvant.
	 *
	 * @return float
	 */
	public function getFondDeCaisseAvant() {
		return $this->fondDeCaisseAvant;
	}

	/**
	 * Set fondDeCaisseApres.
	 *
	 * @param float $fondDeCaisseApres
	 *
	 * @return Festival
	 */
	public function setFondDeCaisseApres( $fondDeCaisseApres ) {
		$this->fondDeCaisseApres = $fondDeCaisseApres;

		return $this;
	}

	/**
	 * Get fondDeCaisseApres.
	 *
	 * @return float
	 */
	public function getFondDeCaisseApres() {
		return $this->fondDeCaisseApres;
	}

	/**
	 * Set chiffreAffaire.
	 *
	 * @param float $chiffreAffaire
	 *
	 * @return Festival
	 */
	public function setChiffreAffaire( $chiffreAffaire ) {
		$this->chiffreAffaire = $chiffreAffaire;

		return $this;
	}

	/**
	 * Get chiffreAffaire.
	 *
	 * @return float
	 */
	public function getChiffreAffaire() {
		return $this->chiffreAffaire;
	}

	/**
	 * Set fraisInscription.
	 *
	 * @param string|null $fraisInscription
	 *
	 * @return Festival
	 */
	public function setFraisInscription( $fraisInscription = null ) {
		$this->fraisInscription = $fraisInscription;

		return $this;
	}

	/**
	 * Get fraisInscription.
	 *
	 * @return string|null
	 */
	public function getFraisInscription() {
		return $this->fraisInscription;
	}

	/**
	 * Set fraisHebergement.
	 *
	 * @param string|null $fraisHebergement
	 *
	 * @return Festival
	 */
	public function setFraisHebergement( $fraisHebergement = null ) {
		$this->fraisHebergement = $fraisHebergement;

		return $this;
	}

	/**
	 * Get fraisHebergement.
	 *
	 * @return string|null
	 */
	public function getFraisHebergement() {
		return $this->fraisHebergement;
	}

	/**
	 * Set fraisTransport.
	 *
	 * @param string|null $fraisTransport
	 *
	 * @return Festival
	 */
	public function setFraisTransport( $fraisTransport = null ) {
		$this->fraisTransport = $fraisTransport;

		return $this;
	}

	/**
	 * Get fraisTransport.
	 *
	 * @return string|null
	 */
	public function getFraisTransport() {
		return $this->fraisTransport;
	}

	/**
	 * Set fraisRepas.
	 *
	 * @param string|null $fraisRepas
	 *
	 * @return Festival
	 */
	public function setFraisRepas( $fraisRepas = null ) {
		$this->fraisRepas = $fraisRepas;

		return $this;
	}

	/**
	 * Get fraisRepas.
	 *
	 * @return string|null
	 */
	public function getFraisRepas() {
		return $this->fraisRepas;
	}
}
