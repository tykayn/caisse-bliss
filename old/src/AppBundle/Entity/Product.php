<?php

namespace CaisseBliss\Entity;

use CaisseBliss\Traits\Commentable;
use CaisseBliss\Traits\Sellable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product {
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	/**
	 * number of items available
	 * @ORM\Column(name="stock_count", type="integer")
	 */
	private $stockCount;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;


	/**
	 * url for image
	 * @ORM\Column(type="string", length=256, nullable=true)
	 */
	private $image;

	/**
	 * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
	 */
	private $category;
	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="products")
	 */
	private $user;
	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductSold", mappedBy="product", cascade={"remove"})
	 */
	private $productsSold;

	use Sellable;
	use Commentable;


	/**
	 * Get id.
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name.
	 *
	 * @param string $name
	 *
	 * @return Product
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set image.
	 *
	 * @param string|null $image
	 *
	 * @return Product
	 */
	public function setImage( $image = null ) {
		$this->image = $image;

		return $this;
	}

	/**
	 * Get image.
	 *
	 * @return string|null
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Set category.
	 *
	 * @param \CaisseBliss\Entity\ProductCategory|null $category
	 *
	 * @return Product
	 */
	public function setCategory( \CaisseBliss\Entity\ProductCategory $category = null ) {
		$this->category = $category;

		return $this;
	}

	/**
	 * Get category.
	 *
	 * @return \CaisseBliss\Entity\ProductCategory|null
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * Set user.
	 *
	 * @param \CaisseBliss\Entity\User|null $user
	 *
	 * @return Product
	 */
	public function setUser( \CaisseBliss\Entity\User $user = null ) {
		$this->user = $user;

		return $this;
	}

	/**
	 * Get user.
	 *
	 * @return \CaisseBliss\Entity\User|null
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * Set stockCount.
	 *
	 * @param int $stockCount
	 *
	 * @return Product
	 */
	public function setStockCount( $stockCount ) {
		$this->stockCount = $stockCount;

		return $this;
	}

	/**
	 * Get stockCount.
	 *
	 * @return int
	 */
	public function getStockCount() {
		return $this->stockCount;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->productsSold = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add productsSold.
	 *
	 * @param \CaisseBliss\Entity\User $productsSold
	 *
	 * @return Product
	 */
	public function addProductsSold( \CaisseBliss\Entity\User $productsSold ) {
		$this->productsSold[] = $productsSold;

		return $this;
	}

	/**
	 * Remove productsSold.
	 *
	 * @param \CaisseBliss\Entity\User $productsSold
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeProductsSold( \CaisseBliss\Entity\User $productsSold ) {
		return $this->productsSold->removeElement( $productsSold );
	}

	/**
	 * Get productsSold.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProductsSold() {
		return $this->productsSold;
	}
}
