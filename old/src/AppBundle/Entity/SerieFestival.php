<?php

namespace CaisseBliss\Entity;

use CaisseBliss\Traits\Commentable;
use Doctrine\ORM\Mapping as ORM;

/**
 * Festival
 *
 * @ORM\Table(name="serieFestival")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FestivalRepository")
 */
class SerieFestival {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * variabilised products sold
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Festival", mappedBy="serieFestival")
     */
    private $festivals;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="seriesFestivals")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFestivals()
    {
        return $this->festivals;
    }

    /**
     * @param mixed $festivals
     */
    public function setFestivals($festivals)
    {
        $this->festivals = $festivals;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param \DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->festivals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add festival.
     *
     * @param \CaisseBliss\Entity\Festival $festival
     *
     * @return SerieFestival
     */
    public function addFestival(\CaisseBliss\Entity\Festival $festival)
    {
        $this->festivals[] = $festival;

        return $this;
    }

    /**
     * Remove festival.
     *
     * @param \CaisseBliss\Entity\Festival $festival
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFestival(\CaisseBliss\Entity\Festival $festival)
    {
        return $this->festivals->removeElement($festival);
    }
}
