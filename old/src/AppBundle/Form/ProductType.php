<?php

namespace CaisseBliss\Form;

use CaisseBliss\Entity\ProductCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ProductType extends AbstractType {
	private $security;

	public function __construct( Security $security ) {
		$this->security = $security;
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder->add( 'name',
			null,
			[
				'label' => 'Nom',
				'attr'  => [
					'autofocus' => true,
				],
			] )
		        ->add( 'image',
			        null,
			        [ 'label' => 'URL pour image' ] )
		        ->add( 'stockCount', null, [ 'label' => 'En stock' ] )
		        ->add( 'price', null, [ 'label' => 'Prix en euros' ] )
		        ->add( 'comment', null, [ 'label' => 'Commentaire' ] );
		$user = $this->security->getUser();
		if ( ! $user ) {
			throw new \LogicException(
				'The SellRecordType cannot be used without an authenticated user!'
			);
		}

		$builder->addEventListener( FormEvents::PRE_SET_DATA,
			function ( FormEvent $event ) {
				$form = $event->getForm();

				$categories = $this->security->getUser()->getCategories();
				// ... add a choice list of friends of the current application user
				$form->add( 'category',
					EntityType::class,
					[
						'class'       => 'AppBundle:ProductCategory',
						'placeholder' => '--- catégories ---',
						'choices'     => $categories,
						'label'       => 'Catégorie',
					] );
			} );

	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( [
			'data_class' => 'AppBundle\Entity\Product',
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'appbundle_product';
	}


}
