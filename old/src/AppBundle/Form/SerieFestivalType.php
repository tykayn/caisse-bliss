<?php

namespace CaisseBliss\Form;

use CaisseBliss\Entity\Festival;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SerieFestivalType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder->add( 'name',
			null,
			[
				'attr' => [
					'autofocus' => true,
				],
			] )
		        ->add( 'dateCreation',
			        DateType::class,
			        [
				        // renders it as a single text box
				        'widget' => 'single_text',
			        ] )
		        ->add( 'festivals', EntityType::class, [ 'class' => Festival::class, 'multiple' => true ] )
			;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions( OptionsResolver $resolver ) {
		$resolver->setDefaults( [
			'data_class' => 'AppBundle\Entity\SerieFestival',
		] );
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'appbundle_seriefestival';
	}


}
