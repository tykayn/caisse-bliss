<?php

namespace CaisseBliss\Service;

use CaisseBliss\Entity\Festival;
use CaisseBliss\Entity\ProductCategory;
use CaisseBliss\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class OwnerService {
	private $em;

	/**
	 * MyFOSUBUserProvider constructor.
	 *
	 * @param EntityManager $em
	 */
	public function __construct(
		ObjectManager $em
	) {
		$this->em = $em;
	}


	/**
	 * add default categories for a newly created user
	 * @return mixed
	 */
	public function setupNewDefaultCategories( User $user ) {

		$categNames = [ 'livres', 'badges', 'dessins' ];
		foreach ( $categNames as $categ_name ) {
			$newCateg = new ProductCategory();
			$newCateg->setName( $categ_name );
			$user->addCategory( $newCateg );
			$newCateg->addUser( $user );
			$this->em->persist( $newCateg );
			$this->em->persist( $user );
		}
		$this->em->flush();

		return $user;
	}

	/**
	 * setup a default festival if needed
	 *
	 * @param User $user
	 *
	 * @return User
	 */
	public function setupNewFestival( User $user ) {

		$activeFestival = $user->getActiveFestival();
		if ( ! $activeFestival ) {
			$activeFestival = $this->em->getRepository( 'AppBundle:Festival' )
			                           ->findOneBy( [ 'user' => $user->getId() ],
				                           [ 'id' => 'desc' ],
				                           0,
				                           1 );
			if ( ! $activeFestival ) {
				$activeFestival = new Festival();
				$activeFestival->setDateCreation( new \DateTime() )
				               ->setName( 'default festival' )
				               ->setChiffreAffaire( 0 )
				               ->setFondDeCaisseAvant( 0 )
				               ->setFondDeCaisseApres( 0 )
				               ->setUser( $user );
			}
			$user->setActiveFestival( $activeFestival );
			$this->em->persist( $activeFestival );
			$this->em->persist( $user );
			$this->em->flush();
		}

		return $user;
	}

	/**
	 * transform categories of a user in an array usable by js
	 * @param User $user
	 *
	 * @return array
	 */
	public function serializeCategoriesOfUser( User $user ) {
		$categRepo  = $this->em->getRepository( 'AppBundle:ProductCategory' );
		$categories = $user->getCategories();
		if ( ! count( $categories ) ) {
			$categories = $defaultCategories = $categRepo->findById( [ 1, 2 ] );
			$user->setCategories( $defaultCategories );
			$this->em->persist( $user );
		}
		$serializedCategories = [];

		foreach ( $categories as $category ) {
			$products = $category->getProducts();
			if ( $products ) {
				$listOfProductsInArray = [];
				foreach ( $products as $product ) {
					$listOfProductsInArray[] = [
						'id'         => $product->getId(),
						'name'       => $product->getName(),
						'category'   => $category->getId(),
						'price'      => 1 * $product->getPrice(),
						'stockCount' => $product->getStockCount(),
					];
				}
				$products = $listOfProductsInArray;
			}
			$serializedCategories[] =
				[
					'id'       => $category->getId(),
					'name'     => $category->getName(),
					'products' => $products ? $products : null,
				];

		}

		return $serializedCategories;
	}

    /**
     * @param User $user
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
	public function serializeExpensesOfUser( User $user ) {
		$expenses = $user->getExpenses();
		$serialized = [];

		foreach ( $expenses as $exp ) {
			$serialized[] =
				[
					'amount'       => $exp->getAmount(),
					'id'       => $exp->getId(),
					'name'     => $exp->getName(),
					'delay'     => $exp->getDelay(),
					'repeat'     => $exp->getRepeatitions(),
					'enabled'     => $exp->getEnabled(),
				];

		}

		return $serialized;
	}
}
