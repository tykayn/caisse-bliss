<?php

namespace CaisseBliss\Security\Core\User;

use CaisseBliss\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Component\Security\Core\User\UserInterface;

class MyFOSUBUserProvider extends BaseFOSUBProvider {
	private $em;

	/**
	 * MyFOSUBUserProvider constructor.
	 *
	 * @param UserManagerInterface $userManager
	 * @param array $properties
	 * @param EntityManager $em
	 */
	public function __construct(
		UserManagerInterface $userManager,
		array $properties,
		EntityManager $em
	) {
		$this->em = $em;
		parent::__construct( $userManager, $properties );
	}

	/**
	 * {@inheritDoc}
	 */
	public
	function connect(
		UserInterface $user,
		UserResponseInterface $response
	) {
// get property from provider configuration by provider name
// , it will return `facebook_id` in that case (see service definition below)
		$property = $this->getProperty( $response );
		$username = $response->getUsername(); // get the unique user identifier

//we "disconnect" previously connected users
		$existingUser = $this->userManager->findUserBy( [ $property => $username ] );
		if ( null !== $existingUser ) {
// set current user id and token to null for disconnect
// ...

			$this->userManager->updateUser( $existingUser );
		}
// we connect current user, set current user id and token
// ...
		$this->userManager->updateUser( $user );
	}

	/**
	 * {@inheritdoc}
	 */
	public
	function loadUserByOAuthUserResponse(
		UserResponseInterface $response
	) {
		$userEmail = $response->getEmail();
		$username  = $response->getRealName();
		$user      = null;
		$em        = $this->em;
		if ( $userEmail ) {
			$user = $this->userManager->findUserByEmail( $userEmail );
		} elseif ( $username ) {
			$user = $this->userManager->findUserByUsername( $username );
		}


// if null just create new user and set it properties
		if ( null === $user ) {

			$user = new User();
			$data = $response->getData();
			if ( ! $userEmail ) {
				$ressourceOwner = $response->getResourceOwner();
				$userEmail      = $username . '@' . $ressourceOwner->getName() . '.com';

			}
			if ( $ressourceOwner == 'twitter' ) {
				$userId = $data[ "id" ];
				$user->setTwitterId( $userId );
			}
			$user
				->setUsername( $username )
				->setPassword( 'sdfvjsdjfsdsjmldfvlkjsdkjlqlkjef56f4sr46g58s6z8r4g+97sr47hz+4' )
				->setEmail( $userEmail )
				->setEmailCanonical( $userEmail );

// ... save user to database

			$em->persist( $user );
			$em->flush();

			return $user;
		}
// else update access token of existing user
		$serviceName = $response->getResourceOwner()->getName();
		$setter      = 'set' . ucfirst( $serviceName ) . 'AccessToken';
		$user->$setter( $response->getAccessToken() );//update access token
		$em->persist( $user );
		$em->flush();

		return $user;
	}
}
