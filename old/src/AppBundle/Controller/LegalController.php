<?php

namespace CaisseBliss\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class LegalController extends Controller {

	private $tokenManager;

	public function __construct( CsrfTokenManagerInterface $tokenManager = null ) {
		$this->tokenManager = $tokenManager;
	}

	/**
	 * @Route("/tos", name="tos")
	 */
	public function tosAction( Request $request ) {

		return $this->render( 'legal/tos.html.twig',
			[] );
	}

	/**
	 * @Route("/privacy", name="privacy")
	 */
	public function privacyAction( Request $request ) {

		// replace this example code with whatever you need
		return $this->render( 'legal/privacy.html.twig',
			[] );
	}


}
