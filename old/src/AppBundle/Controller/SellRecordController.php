<?php

namespace CaisseBliss\Controller;

use CaisseBliss\Entity\SellRecord;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Sellrecord controller.
 *
 * @Route("sellrecord")
 */
class SellRecordController extends Controller {
	/**
	 * Lists all sellRecord entities.
	 *
	 * @Route("/", name="sellrecord_index")
	 * @Method("GET")
	 */
	public function indexAction() {

		$sellRecords = $this->getUser()->getProductsSold();

		return $this->render( 'sellrecord/index.html.twig',
			[
				'sellRecords' => $sellRecords,
			] );
	}

	/**
	 * Creates a new sellRecord entity.
	 *
	 * @Route("/new", name="sellrecord_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$sellRecord = new Sellrecord();
		$sellRecord->setUser( $this->getUser() );
		$form = $this->createForm( 'AppBundle\Form\SellRecordType', $sellRecord );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $sellRecord );
			$em->flush();

			return $this->redirectToRoute( 'sellrecord_show', [ 'date' => $sellRecord->getDate() ] );
		}

		return $this->render( 'sellrecord/new.html.twig',
			[
				'sellRecord' => $sellRecord,
				'form'       => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a sellRecord entity.
	 *
	 * @Route("/{date}", name="sellrecord_show")
	 * @Method("GET")
	 */
	public function showAction( SellRecord $sellRecord ) {
        if ( $sellRecord->getUser()->getId() !== $this->getUser()->getId() ) {
            $this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
        }
		$deleteForm = $this->createDeleteForm( $sellRecord );
		if ( ! $sellRecord->getUser() == $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}

		return $this->render( 'sellrecord/show.html.twig',
			[
				'sellRecord'  => $sellRecord,
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing sellRecord entity.
	 *
	 * @Route("/{date}/edit", name="sellrecord_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, SellRecord $sellRecord ) {
		if ( ! $sellRecord->getUser() == $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$deleteForm = $this->createDeleteForm( $sellRecord );
		$editForm   = $this->createForm( 'AppBundle\Form\SellRecordType', $sellRecord );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'sellrecord_edit', [ 'date' => $sellRecord->getDate() ] );
		}

		return $this->render( 'sellrecord/edit.html.twig',
			[
				'sellRecord'  => $sellRecord,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			] );
	}
	/**
	 * Deletes a sellRecord entity.
	 *
	 * @Route("/delete/{id}", name="sellrecord_delete")
	 */
	public function deleteAction( Request $request, SellRecord $id) {
		$sellRecord = $id;
		if ( $sellRecord->getUser()->getId() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
			$em = $this->getDoctrine()->getManager();
			$em->remove( $sellRecord );
			$em->flush();

		return $this->redirectToRoute( 'history' );
	}

	/**
	 * Creates a form to delete a sellRecord entity.
	 *
	 * @param SellRecord $sellRecord The sellRecord entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( SellRecord $sellRecord ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'sellrecord_delete', [ 'date' => $sellRecord->getDate() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
