<?php

namespace CaisseBliss\Controller;

use CaisseBliss\Entity\SerieFestival;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Seriefestival controller.
 *
 * @Route("seriefestival")
 */
class SerieFestivalController extends Controller
{
    /**
     * Lists all serieFestival entities.
     *
     * @Route("/", name="seriefestival_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $serieFestivals = $em->getRepository('AppBundle:SerieFestival')->findAll();

        return $this->render('seriefestival/index.html.twig', array(
            'serieFestivals' => $serieFestivals,
        ));
    }

    /**
     * Creates a new serieFestival entity.
     *
     * @Route("/new", name="seriefestival_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
	    $usr= $this->getUser();

        $serieFestival = new Seriefestival();
        $serieFestival->setUser($usr);

        $form = $this->createForm('AppBundle\Form\SerieFestivalType', $serieFestival);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($serieFestival);
            $em->flush();

            return $this->redirectToRoute('seriefestival_show', array('id' => $serieFestival->getId()));
        }

        return $this->render('seriefestival/new.html.twig', array(
            'serieFestival' => $serieFestival,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a serieFestival entity.
     *
     * @Route("/{id}", name="seriefestival_show")
     * @Method("GET")
     */
    public function showAction(SerieFestival $serieFestival)
    {
        $deleteForm = $this->createDeleteForm($serieFestival);

        return $this->render('seriefestival/show.html.twig', array(
            'serieFestival' => $serieFestival,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing serieFestival entity.
     *
     * @Route("/{id}/edit", name="seriefestival_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SerieFestival $serieFestival)
    {
        $deleteForm = $this->createDeleteForm($serieFestival);
        $editForm = $this->createForm('AppBundle\Form\SerieFestivalType', $serieFestival);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seriefestival_edit', array('id' => $serieFestival->getId()));
        }

        return $this->render('seriefestival/edit.html.twig', array(
            'serieFestival' => $serieFestival,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a serieFestival entity.
     *
     * @Route("/{id}", name="seriefestival_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SerieFestival $serieFestival)
    {
        $form = $this->createDeleteForm($serieFestival);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($serieFestival);
            $em->flush();
        }

        return $this->redirectToRoute('seriefestival_index');
    }

    /**
     * Creates a form to delete a serieFestival entity.
     *
     * @param SerieFestival $serieFestival The serieFestival entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SerieFestival $serieFestival)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('seriefestival_delete', array('id' => $serieFestival->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
