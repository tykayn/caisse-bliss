# Requirements:
a web server speaking PHP, composer and NPM to install dependencies.
I develop on [Ubuntu](https://ubuntu.org), so here are the steps i recommend:

 
## Install [Composer](https://getcomposer.org/download/) via PHP command
Composer manages dependencies of a lot of php projects. Try these commands or refer to [the download docs of getcomposer.org](https://getcomposer.org/download/)
``` 
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

composer install
```

# Installation:
When you have satisfied all the previous requirements you can install dependencies with [the bash script](/install.sh): 
```bash
bash install.sh
```

## Configure your app parameters
In the [paramters.yml](/app/config/parameters.yml) file, setup your database access (create your DB if necessary). Works with mysql and others via the magic of Doctrine ORM.

```yaml
    database_host: 127.0.0.1
    database_port: null
    database_name: symfony
    database_user: YOURDATABASEUSER
    database_password: YOURDATABASEPASSWORD
```

update schema with doctrine in the command line.
 ```bash
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
```
compile front end assets
```bash
yarn run encore dev --watch
```

access the front with your browser
```bash
php bin/console server:run
firefox http://http://127.0.0.1:8000/
```
enjoy!

#install via Docker ?
I didn't do that for the moment, but you can contribute to this if you like to.
