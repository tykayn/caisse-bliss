CREATE database framadate_api;
CREATE user  framadate_admin@localhost IDENTIFIED by 'framadate-admin-password';
GRANT ALL PRIVILEGES on framadate_api.* to framadate_admin@localhost;

FLUSH PRIVILEGES;