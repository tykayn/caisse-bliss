
![alt text][logo]

Caisse | Cipher Bliss 
====
Self hosted mobile and desktop Cash register and money management for your small business. Made after years of experience in the art of selling home made books and drawings during events like Japan Expo.
![alt_text][screenshot_home]

A Symfony project created on March 23, 2018.

## Features:
* live selling and stock update on phone and desktop
* multi user registration
* managing products, categories
* forecast of expenses
* events
* statistics about earnings
* history of client purchases
* import and export your products and selling history
* have a look at where your expenses go during selling events
* all in a libre software in PHP/JS

---
## [Install](/docs/installation.md)
Have a look at the [Docs to install](/docs/installation.md) your own Caisse. All documentation is in the "docs" folder.
Made By Tykayn/CipherBliss under [AGPL Licence](LICENSE).
## News
Ipublish news about this app here: 
https://www.cipherbliss.com

So here is a tag: https://www.cipherbliss.com/tag/caisse/

You can follow the website with RSS/atom feeds.
https://www.cipherbliss.com/feed/


## Where to find help
Join the discussion at the Matrix Channel of CipherBliss: 
[#cipherbliss:matrix.org](https://matrix.to/#/!jfoYESqTObXYlKAOVM:matrix.org?via=matrix.org)
Or contact the Author:
[Contact Choices here](https://www.cipherbliss.com/contact)


[logo]: https://www.cipherbliss.com/wp-content/uploads/2016/12/rond.png "Logo of CipherBliss company"
[screenshot_home]: https://www.cipherbliss.com/wp-content/uploads/2018/09/caisse_bliss_descritpion_open_source_cipherbliss-1260x709.png "Screenshot of Caisse Bliss"

Hosted on Framagit.org
https://framagit.org/tykayn/caisse-bliss.git
