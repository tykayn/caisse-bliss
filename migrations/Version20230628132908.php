<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230628132908 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE admin (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_account (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A1085643C947C0F');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A1085649903A56B');
        $this->addSql('ALTER TABLE vote DROP FOREIGN KEY FK_5A108564998666D1');
        $this->addSql('ALTER TABLE poll DROP FOREIGN KEY FK_84BCFA457E3C61F9');
        $this->addSql('ALTER TABLE user_product_category DROP FOREIGN KEY FK_B0E9318BA76ED395');
        $this->addSql('ALTER TABLE user_product_category DROP FOREIGN KEY FK_B0E9318BBE6903FD');
        $this->addSql('ALTER TABLE custom_user DROP FOREIGN KEY FK_8CE51EB4DE145A1C');
        $this->addSql('ALTER TABLE product_sold DROP FOREIGN KEY FK_153AFF38A76ED395');
        $this->addSql('ALTER TABLE product_sold DROP FOREIGN KEY FK_153AFF384584665A');
        $this->addSql('ALTER TABLE product_sold DROP FOREIGN KEY FK_153AFF3897DB5DB5');
        $this->addSql('ALTER TABLE serieFestival DROP FOREIGN KEY FK_BCDB0705A76ED395');
        $this->addSql('ALTER TABLE stack_of_votes DROP FOREIGN KEY FK_4809A40D7E3C61F9');
        $this->addSql('ALTER TABLE stack_of_votes DROP FOREIGN KEY FK_4809A40D3C947C0F');
        $this->addSql('ALTER TABLE sell_record DROP FOREIGN KEY FK_F9F12A028AEBAF57');
        $this->addSql('ALTER TABLE sell_record DROP FOREIGN KEY FK_F9F12A02A76ED395');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA76ED395');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7E3C61F9');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C3C947C0F');
        $this->addSql('ALTER TABLE choice DROP FOREIGN KEY FK_C1AB5A923C947C0F');
        $this->addSql('ALTER TABLE expense_kind DROP FOREIGN KEY FK_8F63F2BBA76ED395');
        $this->addSql('ALTER TABLE festival DROP FOREIGN KEY FK_57CF7898DD79D04');
        $this->addSql('ALTER TABLE festival DROP FOREIGN KEY FK_57CF789A76ED395');
        $this->addSql('DROP TABLE vote');
        $this->addSql('DROP TABLE poll');
        $this->addSql('DROP TABLE user_product_category');
        $this->addSql('DROP TABLE owner');
        $this->addSql('DROP TABLE custom_user');
        $this->addSql('DROP TABLE product_sold');
        $this->addSql('DROP TABLE serieFestival');
        $this->addSql('DROP TABLE stack_of_votes');
        $this->addSql('DROP TABLE sell_record');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE choice');
        $this->addSql('DROP TABLE expense_kind');
        $this->addSql('DROP TABLE festival');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vote (id INT AUTO_INCREMENT NOT NULL, choice_id INT NOT NULL, poll_id INT NOT NULL, stacks_of_votes_id INT NOT NULL, value VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_5A108564998666D1 (choice_id), INDEX IDX_5A1085643C947C0F (poll_id), INDEX IDX_5A1085649903A56B (stacks_of_votes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE poll (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, custom_url VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(1000) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, expiracy_date DATETIME NOT NULL, kind VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, allowed_answers LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\', votes_allowed TINYINT(1) DEFAULT NULL, is_zero_knowledge TINYINT(1) DEFAULT NULL, votes_max SMALLINT DEFAULT NULL, choices_max SMALLINT DEFAULT NULL, comments_allowed TINYINT(1) DEFAULT NULL, modification_policy VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, mail_on_comment TINYINT(1) DEFAULT NULL, mail_on_vote TINYINT(1) DEFAULT NULL, hide_results TINYINT(1) DEFAULT NULL, show_result_even_if_passwords TINYINT(1) DEFAULT NULL, password VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, admin_key VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, UNIQUE INDEX UNIQ_84BCFA4559CFBEEF (custom_url), INDEX IDX_84BCFA457E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_product_category (user_id INT NOT NULL, product_category_id INT NOT NULL, INDEX IDX_B0E9318BA76ED395 (user_id), INDEX IDX_B0E9318BBE6903FD (product_category_id), PRIMARY KEY(user_id, product_category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE owner (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, email VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, modifier_token VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, requested_polls_date DATETIME DEFAULT CURRENT_TIMESTAMP, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE custom_user (id INT AUTO_INCREMENT NOT NULL, active_festival_id INT DEFAULT NULL, twitter_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, disqus_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, google_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, mastodon_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, averageMonthlyEarnings DOUBLE PRECISION DEFAULT NULL, disponibility DOUBLE PRECISION DEFAULT NULL, UNIQUE INDEX UNIQ_8CE51EB4DE145A1C (active_festival_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product_sold (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, sell_records_id INT DEFAULT NULL, product_id INT DEFAULT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, image VARCHAR(256) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, price NUMERIC(10, 2) DEFAULT NULL, comment VARCHAR(256) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_153AFF3897DB5DB5 (sell_records_id), INDEX IDX_153AFF384584665A (product_id), INDEX IDX_153AFF38A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE serieFestival (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, dateCreation DATETIME NOT NULL, INDEX IDX_BCDB0705A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE stack_of_votes (id INT AUTO_INCREMENT NOT NULL, poll_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, pseudo VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ip VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_4809A40D3C947C0F (poll_id), INDEX IDX_4809A40D7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE sell_record (id INT AUTO_INCREMENT NOT NULL, festival_id INT DEFAULT NULL, user_id INT DEFAULT NULL, gender VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, date DATETIME NOT NULL, amount NUMERIC(10, 2) NOT NULL, paid_by_client NUMERIC(10, 2) DEFAULT NULL, comment VARCHAR(256) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_F9F12A028AEBAF57 (festival_id), INDEX IDX_F9F12A02A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, user_id INT DEFAULT NULL, stock_count INT NOT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, image VARCHAR(256) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, price NUMERIC(10, 2) DEFAULT NULL, comment VARCHAR(256) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_D34A04ADA76ED395 (user_id), INDEX IDX_D34A04AD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, poll_id INT DEFAULT NULL, pseudo VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, text LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_9474526C3C947C0F (poll_id), INDEX IDX_9474526C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE choice (id INT AUTO_INCREMENT NOT NULL, poll_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, url VARCHAR(1024) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_C1AB5A923C947C0F (poll_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE expense_kind (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, delay INT DEFAULT NULL, enabled TINYINT(1) DEFAULT NULL, repeatitions INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, INDEX IDX_8F63F2BBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE festival (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, serie_festival_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, dateCreation DATETIME NOT NULL, fond_de_caisse_avant DOUBLE PRECISION NOT NULL, fond_de_caisse_apres DOUBLE PRECISION NOT NULL, chiffre_affaire DOUBLE PRECISION NOT NULL, frais_inscription NUMERIC(10, 2) DEFAULT NULL, frais_hebergement NUMERIC(10, 2) DEFAULT NULL, frais_transport NUMERIC(10, 2) DEFAULT NULL, frais_repas NUMERIC(10, 2) DEFAULT NULL, comment VARCHAR(256) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_57CF7898DD79D04 (serie_festival_id), INDEX IDX_57CF789A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085643C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A1085649903A56B FOREIGN KEY (stacks_of_votes_id) REFERENCES stack_of_votes (id)');
        $this->addSql('ALTER TABLE vote ADD CONSTRAINT FK_5A108564998666D1 FOREIGN KEY (choice_id) REFERENCES choice (id)');
        $this->addSql('ALTER TABLE poll ADD CONSTRAINT FK_84BCFA457E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id)');
        $this->addSql('ALTER TABLE user_product_category ADD CONSTRAINT FK_B0E9318BA76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_product_category ADD CONSTRAINT FK_B0E9318BBE6903FD FOREIGN KEY (product_category_id) REFERENCES product_category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE custom_user ADD CONSTRAINT FK_8CE51EB4DE145A1C FOREIGN KEY (active_festival_id) REFERENCES festival (id)');
        $this->addSql('ALTER TABLE product_sold ADD CONSTRAINT FK_153AFF38A76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('ALTER TABLE product_sold ADD CONSTRAINT FK_153AFF384584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_sold ADD CONSTRAINT FK_153AFF3897DB5DB5 FOREIGN KEY (sell_records_id) REFERENCES sell_record (id)');
        $this->addSql('ALTER TABLE serieFestival ADD CONSTRAINT FK_BCDB0705A76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('ALTER TABLE stack_of_votes ADD CONSTRAINT FK_4809A40D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id)');
        $this->addSql('ALTER TABLE stack_of_votes ADD CONSTRAINT FK_4809A40D3C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE sell_record ADD CONSTRAINT FK_F9F12A028AEBAF57 FOREIGN KEY (festival_id) REFERENCES festival (id)');
        $this->addSql('ALTER TABLE sell_record ADD CONSTRAINT FK_F9F12A02A76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES owner (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C3C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE choice ADD CONSTRAINT FK_C1AB5A923C947C0F FOREIGN KEY (poll_id) REFERENCES poll (id)');
        $this->addSql('ALTER TABLE expense_kind ADD CONSTRAINT FK_8F63F2BBA76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('ALTER TABLE festival ADD CONSTRAINT FK_57CF7898DD79D04 FOREIGN KEY (serie_festival_id) REFERENCES serieFestival (id)');
        $this->addSql('ALTER TABLE festival ADD CONSTRAINT FK_57CF789A76ED395 FOREIGN KEY (user_id) REFERENCES custom_user (id)');
        $this->addSql('DROP TABLE admin');
        $this->addSql('DROP TABLE user_account');
    }
}
